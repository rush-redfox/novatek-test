// Utilities
import { defineStore } from 'pinia'

export const useAppStore = defineStore('app', {
	state: () => ({
		counters: [
			0, 0, 0
		]
	}),
})
