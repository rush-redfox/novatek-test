// Composables
import { createRouter, createWebHistory } from 'vue-router'
import One from '@/components/One.vue';
import Two from '@/components/Two.vue';
import Three from '@/components/Three.vue';

const routes = [
	{
		path: '',
		name: 'Default',
		component: One
	},
	{
		path: '/one',
		name: 'One',
		component: One
	},
	{
		path: '/two',
		name: 'Two',
		component: Two
	},
	{
		path: '/three',
		name: 'Three',
		component: Three
	},
]

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes,
})

export default router
